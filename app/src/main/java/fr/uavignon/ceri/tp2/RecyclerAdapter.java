package fr.uavignon.ceri.tp2;


import android.app.Activity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.view.ActionMode;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class RecyclerAdapter extends RecyclerView.Adapter<fr.uavignon.ceri.tp2.RecyclerAdapter.ViewHolder> {

    private static final String TAG = fr.uavignon.ceri.tp2.RecyclerAdapter.class.getSimpleName();

    private ListViewModel listViewModel;
    private List<Book> bookList;
    private Activity mActivity;
    android.view.ActionMode mActionMode;

    public RecyclerAdapter(Activity mActivity, ListViewModel listViewModel) {
        this.mActivity = mActivity;
        this.listViewModel = listViewModel;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.itemTitle.setText(bookList.get(i).getTitle());
        viewHolder.itemDetail.setText(bookList.get(i).getAuthors());
    }

    @Override
    public int getItemCount() {
        if(bookList != null)
            return bookList.size();
        else return 0;

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView itemTitle;
        TextView itemDetail;


        ViewHolder(View itemView) {
            super(itemView);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);



            itemView.setOnClickListener((View.OnClickListener) v -> {
                long id = bookList.get((int)getAdapterPosition()).getId();
                ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
                action.setBookNum(id);
                Navigation.findNavController(v).navigate(action);


            });

            android.view.ActionMode.Callback mActionModeCallback = new android.view.ActionMode.Callback() {
                @Override
                public boolean onCreateActionMode(android.view.ActionMode actionMode, Menu menu) {
                    actionMode.getMenuInflater().inflate(R.menu.context_menu, menu);
                    return true;
                }

                @Override
                public boolean onPrepareActionMode(android.view.ActionMode actionMode, Menu menu) {
                    return false;
                }

                @Override
                public boolean onActionItemClicked(android.view.ActionMode actionMode, MenuItem menuItem) {
                    if(menuItem.getItemId() == R.id.delete_book){
                        listViewModel.deleteBook(bookList.get((int)getAdapterPosition()));
                    }
                    actionMode.finish();
                    return true;
                }

                @Override
                public void onDestroyActionMode(android.view.ActionMode actionMode) {
                    mActionMode = null;
                }
            };


            itemView.setOnLongClickListener((View.OnLongClickListener) v ->{
                if(mActionMode != null) {
                    return false;
                }

                mActionMode = mActivity.startActionMode( mActionModeCallback);
                return true;
            });



        }
    }

    public void setBookList(List<Book> bookList) {
        this.bookList = bookList;
    }


}