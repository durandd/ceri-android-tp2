package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;

public class ListFragment extends Fragment {

    ListViewModel listViewModel;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager layoutManager;
    RecyclerAdapter adapter;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.recyclerView);

        recyclerSetup(); // association de l'adaptateur àla RecyclerView
        listenerSetup(view); // enregistrement du fragment comme
        observerSetup(); // observateur du ViewModel



    }

    private void recyclerSetup() {
        listViewModel = new ViewModelProvider(this).get(ListViewModel.class);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new RecyclerAdapter(getActivity(),listViewModel);
        recyclerView.setAdapter(adapter);
    }

    private void observerSetup() {
        listViewModel.getAllBooks().observe(getViewLifecycleOwner(), books -> {
            adapter.setBookList(listViewModel.getAllBooks().getValue());
            adapter.notifyDataSetChanged();
        } );
    }

    private void listenerSetup(View view) {
        FloatingActionButton fab = view.findViewById(R.id.fab);
        fab.setOnClickListener(v -> {

            ListFragmentDirections.ActionFirstFragmentToSecondFragment action = ListFragmentDirections.actionFirstFragmentToSecondFragment();
            action.setBookNum(-1);
            Navigation.findNavController(v).navigate(action);
        });

    }
}