package fr.uavignon.ceri.tp2;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

import com.google.android.material.snackbar.Snackbar;

import fr.uavignon.ceri.tp2.data.Book;

public class DetailFragment extends Fragment {

    private EditText textTitle, textAuthors, textYear, textGenres, textPublisher;
    private DetailViewModel detailViewModel;
    private Book book;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Get selected book
        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
        detailViewModel = new ViewModelProvider(this).get(DetailViewModel.class);
        long bookId = (int)args.getBookNum();


        textTitle = (EditText) view.findViewById(R.id.nameBook);
        textAuthors = (EditText) view.findViewById(R.id.editAuthors);
        textYear = (EditText) view.findViewById(R.id.editYear);
        textGenres = (EditText) view.findViewById(R.id.editGenres);
        textPublisher = (EditText) view.findViewById(R.id.editPublisher);

        if(bookId>=0){
            Book book = detailViewModel.getBook(bookId).getValue();
            setTextFields(book);
        }

        listenerSetup(view); // enregistrement du fragment comme
        observerSetup(bookId); // observateur du ViewModel


    }

    private void listenerSetup(View view) {
        view.findViewById(R.id.buttonBack).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                        .navigate(R.id.action_SecondFragment_to_FirstFragment);
            }
        });

        view.findViewById(R.id.buttonUpdate).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String title = textTitle.getText().toString();
                String authors = textAuthors.getText().toString();
                String genres = textGenres.getText().toString();
                String year = textYear.getText().toString();
                String publisher = textPublisher.getText().toString();

                if(title.isEmpty() || authors.isEmpty()){
                    Snackbar.make(view, "Veuillez renseigner au moins le titre et l'auteur",
                            Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show();
                }
                else{
                    if(book!= null){
                        book.setTitle(title);
                        book.setAuthors(authors);
                        book.setYear(year);
                        book.setGenres(genres);
                        book.setPublisher(publisher);
                    }
                    else {
                        book = new Book(title, authors, year, genres, publisher);
                    }
                    detailViewModel.insertOrUpdateBook(book);
                    Snackbar.make(view, "La base de donnée a été mise à jour",
                            Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                    NavHostFragment.findNavController(fr.uavignon.ceri.tp2.DetailFragment.this)
                            .navigate(R.id.action_SecondFragment_to_FirstFragment);
                }
            }
        });
    }

    private void observerSetup(long bookId) {

        detailViewModel.getBook(bookId).observe(getViewLifecycleOwner(), book -> {
            if(bookId>=0) {
                this.book = book;
                setTextFields(book);
            }
        });
    }

    private void setTextFields(Book book) {
        textTitle.setText(book.getTitle());
        textAuthors.setText(book.getAuthors());
        textYear.setText(book.getYear());
        textGenres.setText(book.getGenres());
        textPublisher.setText(book.getPublisher());
    }


}