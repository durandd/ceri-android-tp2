package fr.uavignon.ceri.tp2;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.List;

import fr.uavignon.ceri.tp2.data.Book;
import fr.uavignon.ceri.tp2.data.BookRepository;

public class DetailViewModel extends AndroidViewModel {
    private BookRepository repository;

    public DetailViewModel(@NonNull Application application) {
        super(application);
        repository = new BookRepository(application);
    }

    MutableLiveData<Book> getBook(long id){
        repository.getBook(id);
        return repository.getSelectedBook();
    }

    public void insertOrUpdateBook(Book book){
        if(book.getId()>0)
            repository.updateBook(book);
        else
            repository.insertBook(book);
    }

}
